package vn.tcbs.tool.modelserver.controller;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.modelserver.entity.ReportItem;
import vn.tcbs.tool.modelserver.entity.TemplateData;
import vn.tcbs.tool.modelserver.service.ReportService;
import vn.tcbs.tool.modelserver.service.TemplateDataService;

@RestController
@RequestMapping(value = "/api")
public class ApiController {

	@Autowired
	private TemplateDataService templateDataService;
	@Autowired
	ReportService reportService;

	private static final Logger logger = Logger.getLogger(ApiController.class);

	@RequestMapping(value = "template/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseData<TemplateData> addTemplate(
			@RequestBody TemplateData data) {

		/*
		 * TemplateData obj = new TemplateData(); obj.setTemplateData(data); int
		 * id = templateDataService.add(obj);
		 */
		// return new ResponseData<String>(0, id + "", "SUCCESS");
		data = templateDataService.save(data);
		return new ResponseData<TemplateData>(0, data, "SUCCESS");
	}

	@RequestMapping(value = "template/load", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseData<TemplateData> loadTemplate(
			@RequestBody TemplateData tpData) {
		TemplateData obj = templateDataService.getById(tpData
				.getTemplateDataId());
		return new ResponseData<TemplateData>(0, obj, "SUCCESS");
	}

	@RequestMapping(value = "company/financial-report/{code}-{year}-{quarter}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseData<ReportItem[]> financialReport(
			@PathVariable String code, @PathVariable int year,
			@PathVariable int quarter) {
		HashMap<String, Object> restData = reportService.getReportByCompany(
				code, year, quarter);

		ReportItem[] ret = null;
		if ((int) restData.get("status") == 0) {
			HashMap<String, Object> data = (HashMap<String, Object>) restData.get("data");
			HashMap<String, Object> title = new HashMap<String, Object>();
			title.put("F1", "Hello");
			ret = new ReportItem[data.size()];
			int count = 0;
			for (Entry<String, Object> obj : data.entrySet()) {
				ret[count] = new ReportItem(obj.getKey(), "A", obj.getValue());
				count++;
			}
		}
		return new ResponseData<ReportItem[]>(0, ret, "SUCCESS");
	}
}