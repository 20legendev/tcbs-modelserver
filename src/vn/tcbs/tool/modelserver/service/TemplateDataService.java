package vn.tcbs.tool.modelserver.service;

import vn.tcbs.tool.modelserver.entity.TemplateData;

public interface TemplateDataService {

	public TemplateData getById(int id);

	public TemplateData save(TemplateData data);

	public boolean update(TemplateData data);
}
