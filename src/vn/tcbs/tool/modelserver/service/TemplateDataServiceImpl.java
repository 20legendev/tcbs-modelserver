package vn.tcbs.tool.modelserver.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.tool.modelserver.dao.TemplateDataDAO;
import vn.tcbs.tool.modelserver.entity.TemplateData;

@Service("templateDataService")
public class TemplateDataServiceImpl implements TemplateDataService {

	@Autowired
	private TemplateDataDAO templateDataDAO;
	private static final Logger logger = Logger
			.getLogger(TemplateDataServiceImpl.class);

	@Override
	public boolean update(TemplateData data) {
		// TODO Auto-generated method stub
		templateDataDAO.update(data);
		return false;
	}

	@Override
	public TemplateData getById(int id) {
		// TODO Auto-generated method stub
		return templateDataDAO.getById(id);
	}

	@Override
	public TemplateData save(TemplateData data) {
		// TODO Auto-generated method stub
		TemplateData obj = templateDataDAO.getById(data.getTemplateDataId());
		if (obj == null) {
			templateDataDAO.add(data);
		} else {
			templateDataDAO.update(data);
		}
		return data;
	}

}
