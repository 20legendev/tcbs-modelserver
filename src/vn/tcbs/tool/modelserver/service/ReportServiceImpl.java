package vn.tcbs.tool.modelserver.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import vn.tcbs.tool.modelserver.entity.ReportField;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

	@Override
	public HashMap<String, Object> getReportByCompany(String code, int year,
			int quarter) {
		RestTemplate restTemplate = new RestTemplate();
		HashMap<String, Object> restData = restTemplate.getForObject(
				"http://localhost:8080/stoxdata-service/api/financial-report/"
						+ code + "-" + year + "-" + quarter, HashMap.class);
		return restData;
	}

	@Override
	public List<ReportField> getReportField() {
		// TODO Auto-generated method stub
		return null;
	}

}
