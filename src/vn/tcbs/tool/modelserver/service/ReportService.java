package vn.tcbs.tool.modelserver.service;

import java.util.HashMap;
import java.util.List;

import vn.tcbs.tool.modelserver.entity.ReportField;

public interface ReportService {

	public HashMap<String, Object> getReportByCompany(String code, int year, int quarter);
	public List<ReportField> getReportField();
}
