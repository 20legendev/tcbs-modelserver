package vn.tcbs.tool.modelserver.domain;

import java.io.Serializable;

public class Message implements Serializable {

	String name;
	String text;

	public Message(String name, String text) {
		this.name = name;
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}
	
	public String toString(){
		return "Kien";
	}

}