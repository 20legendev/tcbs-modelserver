package vn.tcbs.tool.modelserver.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the template_data database table.
 * 
 */
@Entity
@Table(name = "template_data")
@NamedQuery(name = "TemplateData.findAll", query = "SELECT t FROM TemplateData t")
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class TemplateData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7944690836794266650L;

	@Id
	@Column(name = "template_data_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int templateDataId;

	@Type(type = "timestamp")
	@Column(name = "last_updated", insertable = false)
	private Timestamp lastUpdated;

	@Lob
	@Column(name = "template_data")
	private String templateData;

	public TemplateData() {
	}

	public int getTemplateDataId() {
		return this.templateDataId;
	}

	public void setTemplateDataId(int templateDataId) {
		this.templateDataId = templateDataId;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getTemplateData() {
		return this.templateData;
	}

	public void setTemplateData(String templateData) {
		this.templateData = templateData;
	}

}