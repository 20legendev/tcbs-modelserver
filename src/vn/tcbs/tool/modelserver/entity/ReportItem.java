package vn.tcbs.tool.modelserver.entity;

public class ReportItem<T> {
	private String code;
	private String title;
	private T value;

	public ReportItem(String code, String title, T value) {
		this.setCode(code);
		this.setTitle(title);
		this.setValue(value);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}
