package vn.tcbs.tool.modelserver.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the report_field database table.
 * 
 */
@Entity
@Table(name = "report_field")
@NamedQuery(name = "ReportField.findAll", query = "SELECT r FROM ReportField r")
public class ReportField implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "field_id")
	private int fieldId;

	@Lob
	@Column(name = "field_code")
	private String fieldCode;

	@Column(name = "field_name")
	private Object fieldName;

	public ReportField() {
	}

	public int getFieldId() {
		return this.fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldCode() {
		return this.fieldCode;
	}

	public void setFieldCode(String fieldCode) {
		this.fieldCode = fieldCode;
	}

	public Object getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(Object fieldName) {
		this.fieldName = fieldName;
	}

}