package vn.tcbs.tool.modelserver.dao;

import java.util.List;

import vn.tcbs.tool.modelserver.entity.ReportField;

public interface ReportFieldDAO {
	public List<ReportField> findAll();
}