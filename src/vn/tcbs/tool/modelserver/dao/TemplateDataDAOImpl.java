package vn.tcbs.tool.modelserver.dao;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.modelserver.entity.TemplateData;

@Repository("templateDataDAO")
@Transactional
public class TemplateDataDAOImpl implements TemplateDataDAO {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	@Override
	public TemplateData save(TemplateData data) {
		// Logger.getLogger(this.getClass()).debug("KIEN " + data.toString());
		sessionFactory.getCurrentSession().saveOrUpdate("TemplateData", data);
		return data;
	}

	@Override
	public boolean update(TemplateData data) {
		sessionFactory.getCurrentSession().update(data);
		return true;
	}

	@Override
	public TemplateData getById(int id) {
		TemplateData obj = (TemplateData) sessionFactory.getCurrentSession()
				.get(TemplateData.class, id);
		return obj;
	}

	@Override
	public void add(TemplateData data) {
		sessionFactory.getCurrentSession().save(data);
	}

}