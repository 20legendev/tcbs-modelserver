package vn.tcbs.tool.modelserver.dao;

import vn.tcbs.tool.modelserver.entity.TemplateData;

public interface TemplateDataDAO {

	public TemplateData getById(int id);

	public TemplateData save(TemplateData data);

	public boolean update(TemplateData data);

	public void add(TemplateData data);
}