package vn.tcbs.tool.modelserver.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.modelserver.entity.ReportField;

@Repository("reportFieldDAO")
@Transactional
public class ReportFieldDAOImpl extends BaseDAO implements ReportFieldDAO {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	@Override
	public List<ReportField> findAll() {
		// TODO Auto-generated method stub
		Query q = this.getSession().createQuery(
				"from" + this.getClass().getName());
		q.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<ReportField> result = q.list();
		return result;
	}

}