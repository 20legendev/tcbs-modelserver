package vn.tcbs.tool.base;

import java.io.Serializable;
import java.util.Map;

public class ResponseData<T> implements Serializable {

	private static final long serialVersionUID = 3159038893123905511L;

	private int status;
	private String msg;
	private T data;

	public ResponseData() {

	}

	public ResponseData(int status, T page, String msg) {
		this.setStatus(status);
		this.setMsg(msg);
		this.setData(page);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
