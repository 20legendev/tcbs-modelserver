/*
 Navicat Premium Data Transfer

 Source Server         : tcbs-test-server
 Source Server Type    : SQL Server
 Source Server Version : 10501600
 Source Host           : 192.168.9.45
 Source Database       : ModelServer
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 10501600
 File Encoding         : utf-8

 Date: 04/24/2015 16:13:01 PM
*/

-- ----------------------------
--  Table structure for [dbo].[template_data]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[template_data]') AND type IN ('U'))
	DROP TABLE [dbo].[template_data]
GO
CREATE TABLE [dbo].[template_data] (
	[template_data] text COLLATE Vietnamese_100_CI_AI NULL,
	[last_updated] datetime NULL DEFAULT (getdate()),
	[template_data_id] int IDENTITY(1,1) NOT NULL
)
GO

-- ----------------------------
--  Records of [dbo].[template_data]
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [dbo].[template_data] ON
GO
INSERT INTO [dbo].[template_data] ([template_data], [last_updated], [template_data_id]) VALUES ('{"C12":{},"C14":{"style":{}},"C13":{"style":{}},"C17":{"command":"=IFERROR(C11/AVERAGE(C106:C106),\"\")","style":{}},"C11":{"style":{"background-color":"#ffdede"}},"D18":{"style":{}},"D19":{"style":{}},"D20":{"style":{}},"D11":{"style":{"background-color":"#ffe0e0"}},"C20":{"style":{},"command":"=C23-C13"},"D16":{"style":{}},"D13":{"style":{}},"D17":{"style":{}},"C16":{"style":{}},"C18":{"style":{},"command":"=IFERROR(C101/C13,\"\")"},"E12":{"style":{}},"E13":{"style":{}},"E11":{"style":{"background-color":"#ffe3e3"}},"C19":{"style":{},"command":"=IFERROR(-C13/C11,\"\")"},"C22":{"style":{},"command":"=IFERROR(C20/C23,\"\")"},"C21":{"style":{},"command":"=IFERROR(C20/-C11,\"\")"},"C23":{"style":{"background-color":"#ffeded"}},"D22":{"style":{}},"C24":{"style":{},"command":"=IFERROR(-C29/C11,\"\")"},"C27":{"style":{},"command":"SYM"},"C26":{"style":{},"command":"=IFERROR(-C29/C11,\"\")"},"C29":{"style":{}},"C25":{"style":{},"command":"C11+C13"},"E9":{"style":{}},"E17":{"style":{}},"E20":{"style":{}},"D25":{"style":{}}}', null, '15');
GO
SET IDENTITY_INSERT [dbo].[template_data] OFF
GO
COMMIT
GO


-- ----------------------------
--  Primary key structure for table [dbo].[template_data]
-- ----------------------------
ALTER TABLE [dbo].[template_data] ADD
	CONSTRAINT [PK__template__BE44E079117F9D94] PRIMARY KEY CLUSTERED ([template_data_id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
GO

-- ----------------------------
--  Options for table [dbo].[template_data]
-- ----------------------------
ALTER TABLE [dbo].[template_data] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[template_data]', RESEED, 15)
GO

